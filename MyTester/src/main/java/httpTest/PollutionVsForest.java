package httpTest;

import com.google.gson.JsonArray;

//te

public class PollutionVsForest implements Analysis{
	
	private JsonArray jsonArray;
	private JsonArray jsonArray2;
	private Double[] forestarea;
	private Double[] polutionExposure;
	private String country;
	private int from;
	private int to;
	
	
	public PollutionVsForest (JsonArray pollution, JsonArray forest ) {
		jsonArray = pollution;
		jsonArray2 = forest;
	}
	
	public Double[] getPollution() throws Exception {
		int year;																											//variable to track the current year																											
		double averagPollution = 0;																						//variable to track the current average forest area																						
		int sizeOfResults = jsonArray.get(1).getAsJsonArray().size(); 
		polutionExposure = new Double[sizeOfResults];
		country = jsonArray.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("country").getAsJsonObject().get("value").getAsString();
		to = jsonArray.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("date").getAsInt();
		from = jsonArray.get(1).getAsJsonArray().get(sizeOfResults - 1)
				.getAsJsonObject().get("date").getAsInt();
		
		for (int i = 0; i < sizeOfResults; i++) {
			year = jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();
			if (jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {
				averagPollution = 0;
			}
			else {
				averagPollution = jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();
			}
			polutionExposure[i] = averagPollution;
		}
		return polutionExposure;
		

	}
	
	public Double[] getForest() throws Exception {
		
		int year;																											//variable to track the current year																											
		double averageForestArea = 0;																						//variable to track the current average forest area																						
		double cummulativeForestArea = 0;
		int sizeOfResults = jsonArray2.get(1).getAsJsonArray().size();
		forestarea = new Double[sizeOfResults];
		country = jsonArray2.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("country").getAsJsonObject().get("value").getAsString();
		to = jsonArray2.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("date").getAsInt();
		from = jsonArray2.get(1).getAsJsonArray().get(sizeOfResults - 1)
				.getAsJsonObject().get("date").getAsInt();
		
		for (int i = 0; i < sizeOfResults; i++) {
			year = jsonArray2.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();
			if (jsonArray2.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {
				throw new Exception("no data for year: " + year);
			}
			else {
				averageForestArea = jsonArray2.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();
			}
			forestarea[i] = averageForestArea;
		}
		return forestarea;
	}
	

	
	public void performAnalysis() throws Exception {
		Double[] pollutionData = getPollution();
		Double[] forestData = getForest();

		for (int i = 0; i < forestData.length; i++) {
			
			if (pollutionData[i] == 0) {
				System.out.println("Analysis not avaliable for " + ""+ (from + i));
			}
			else {
				System.out.println("In " + (from + i) + "the PM2.5 air pollution, mean annual exposure is " + pollutionData[i] + "and the forest area is "+ forestData[i]);
			}		
		}	
		
	}

}
