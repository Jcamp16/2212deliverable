package httpTest;

import com.google.gson.JsonArray;

public class AverageForestArea implements Analysis{
	
	public static JsonArray jsonArray;
	public Double[] area;
	public String country;
	public int from;
	public int to;
	
	//constructor of the class. the AverageForestArea class receives a json object for which to analyze
	public AverageForestArea(JsonArray jsonArr) {
		jsonArray = jsonArr;
	}
	
	/*
	 * Processes the json object. This entails calculating required variables and identifying required values
	 * for the viewer to create its visualizations
	 * the AverageForestArea analysis type requires no calculations. Only for the following values to be identified:
	 * - average forest area for each specified year -> area[]
	 * - country for which the data is on            -> country
	 * - start date from which data is pulled        -> from
	 * - end date from which data is pulled          -> to
	 */
	public void performAnalysis() throws Exception {
		
		int year;																											//variable to track the current year																											
		double averageForestArea = 0;																						//variable to track the current average forest area																						
		double cummulativeForestArea = 0;
		int sizeOfResults = jsonArray.get(1).getAsJsonArray().size();
		area = new Double[sizeOfResults];
		country = jsonArray.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("country").getAsJsonObject().get("value").getAsString();
		from = jsonArray.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("date").getAsInt();
		to = jsonArray.get(1).getAsJsonArray().get(sizeOfResults - 1)
				.getAsJsonObject().get("date").getAsInt();
		
		for (int i = 0; i < sizeOfResults; i++) {
			year = jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();
			if (jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {
				throw new Exception("no data for year: " + year);
			}
			else {
				averageForestArea = jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();
			}
			area[i] = averageForestArea;
				

			System.out.println("Average forest area for : " + year + " is " + averageForestArea);
			cummulativeForestArea = cummulativeForestArea + averageForestArea;
		}
		System.out.println(
				"The average average forest area over the selected years is " + cummulativeForestArea / sizeOfResults);
	}
}
