package httpTest;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ReportViewer implements Viewer{
	JPanel west;
	public ReportViewer(JPanel _west) {
		west = _west;
		String analysis, reportMessage, reportMessage2;

		JTextArea report = new JTextArea();

		report.setEditable(false);

		report.setPreferredSize(new Dimension(400, 300));

		report.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));

		report.setBackground(Color.white);

		

		analysis =  "\n ============================== \n" ;



		reportMessage = "";



		reportMessage2 = "";



		report.setText(analysis);

		JScrollPane outputScrollPane = new JScrollPane(report);

		west.add(outputScrollPane);

	}
}
