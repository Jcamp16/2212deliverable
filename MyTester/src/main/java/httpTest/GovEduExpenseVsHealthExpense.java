package httpTest;

import com.google.gson.JsonArray;

public class GovEduExpenseVsHealthExpense implements Analysis{
	
	public JsonArray eduJson;
	public JsonArray healthJson;
	public Double[] education;
	public Double[] health;
	public String country;
	public int from;
	public int to;
	
	//constructor of the class. the AverageForestArea class receives a json object for which to analyze
	public GovEduExpenseVsHealthExpense(JsonArray edu, JsonArray health) {
		eduJson = edu;
		healthJson = health;
	}
	
	/*
	 * Processes the json object. This entails calculating required variables and identifying required values
	 * for the viewer to create its visualizations
	 * the AverageForestArea analysis type requires no calculations. Only for the following values to be identified:
	 * - average forest area for each specified year -> area[]
	 * - country for which the data is on            -> country
	 * - start date from which data is pulled        -> from
	 * - end date from which data is pulled          -> to
	 */
	public void performAnalysis() throws Exception {
		
		int year;																											//variable to track the current year																											
		double averageEdu = 0;	
		double averageHealth = 0;																				
		double cummulativeEdu = 0;
		double cummulativeHealth = 0;
		int sizeOfResults = eduJson.get(1).getAsJsonArray().size();
		education = new Double[sizeOfResults];
		health = new Double[sizeOfResults];
		country = eduJson.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("country").getAsJsonObject().get("value").getAsString();
		from = eduJson.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("date").getAsInt();
		to = eduJson.get(1).getAsJsonArray().get(sizeOfResults - 1)
				.getAsJsonObject().get("date").getAsInt();
		
		for (int i = 0; i < sizeOfResults; i++) {
			year = eduJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();
			if (eduJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull() ||
					healthJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {
				throw new Exception("no data for year: " + year);
			}
			else {
				averageEdu = eduJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();
				averageHealth = healthJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();
			}
			education[i] = averageEdu;
			health[i] = averageHealth;
				

			System.out.println("ratio of government spending on education to health for : " + year + " is " + averageEdu + ":" + averageHealth);
			cummulativeEdu = cummulativeEdu + averageEdu;
			cummulativeHealth = cummulativeHealth + averageHealth;
		}
		System.out.println(
				"The average ratio of government spending on education to health for selected years is " + cummulativeEdu / sizeOfResults + ":" + cummulativeHealth / sizeOfResults);
	}
}
