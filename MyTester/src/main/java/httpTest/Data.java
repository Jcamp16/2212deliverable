package httpTest;

public class Data {
	private String country;
	private int from;
	private int to;
	
	public Data(String _country, int _from, int _to) {
		country = _country;
		from = _from;
		to = _to;
	}
	
	public String getCountry(){
		return country;
	}
	
	public int getFrom(){
		return from;
	}
	
	public int getTo(){
		return to;
	}
}
