package httpTest;

//te

import com.google.gson.JsonArray;
import java.lang.Math; // Needed to use Math.round()


public class CO2vsGDP implements Analysis {
	
	private JsonArray jsonArray;
	private JsonArray jsonArray2;
	private Double[] GDP;
	private Double[] CO2;
	private String country;
	private int from;
	private int to;
	
	public CO2vsGDP (JsonArray CO2Emission, JsonArray GdpPerCapita ) {
		jsonArray = CO2Emission;
		jsonArray2 = GdpPerCapita;
		
	}
	
	public Double[] getCO2() throws Exception {
		int year;																											//variable to track the current year																											
		double CO2emmision = 0;																						//variable to track the current average forest area																						
		int sizeOfResults = jsonArray.get(1).getAsJsonArray().size(); 
		CO2 = new Double[sizeOfResults];
		country = jsonArray.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("country").getAsJsonObject().get("value").getAsString();
		to = jsonArray.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("date").getAsInt();
		from = jsonArray.get(1).getAsJsonArray().get(sizeOfResults - 1)
				.getAsJsonObject().get("date").getAsInt();
		
		for (int i = 0; i < sizeOfResults; i++) {
			year = jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();
			if (jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {
				throw new Exception("no data for year: " + year);			}
			else {
				CO2emmision = jsonArray.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();
			}
			CO2[i] = CO2emmision;
		}
		return CO2;	

	}
	
	public Double[] getGDP() throws Exception {
		int year;																											//variable to track the current year																											
		double GDPperCapita = 0;																						//variable to track the current average forest area																						
		int sizeOfResults = jsonArray2.get(1).getAsJsonArray().size(); 
		GDP = new Double[sizeOfResults];
		country = jsonArray2.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("country").getAsJsonObject().get("value").getAsString();
		to = jsonArray2.get(1).getAsJsonArray().get(0).getAsJsonObject()
				.get("date").getAsInt();
		from = jsonArray2.get(1).getAsJsonArray().get(sizeOfResults - 1)
				.getAsJsonObject().get("date").getAsInt();
		for (int i = 0; i < sizeOfResults; i++) {
			year = jsonArray2.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();
			if (jsonArray2.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {
				throw new Exception("no data for year: " + year);			}
			else {
				GDPperCapita = jsonArray2.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();
			}
			GDP[i] = GDPperCapita;
		}
		return GDP;	

	}
 
	public void performAnalysis() throws Exception {
		Double[] CO2data = getCO2();
		Double[] GDPdata = getGDP();
		Double[] CO2vsGDP = new Double [CO2data.length];

		for (int i = 0; i < CO2vsGDP.length; i++) {

			double data = CO2data[i]/GDPdata[i];
			CO2vsGDP[i] = data;
			
			if (CO2vsGDP[i] == 0) {
				System.out.println("Analysis not avaliable for " + ""+ (from + i));
			}
			else {
				System.out.println("ratio of CO2 emission and GDP per capita for " + country  + " in " + (from + i) + " is" + " " + ((CO2vsGDP[i]*100.0)/100.0));
			}		
		}	
		
	}	
		
	

}
