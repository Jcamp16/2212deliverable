package httpTest;



import java.awt.BasicStroke;

import java.awt.BorderLayout;

import java.awt.Color;

import java.awt.Dimension;

import java.awt.Font;

import java.awt.GridLayout;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import java.awt.event.ComponentEvent;

import java.awt.event.ComponentListener;

import java.awt.event.ItemEvent;

import java.awt.event.ItemListener;

import java.util.Vector;

import java.util.ArrayList;



import javax.swing.Action;

import javax.swing.BorderFactory;

import javax.swing.JButton;

import javax.swing.JComboBox;

import javax.swing.JFrame;

import javax.swing.JLabel;

import javax.swing.JPanel;

import javax.swing.JScrollPane;

import javax.swing.JTextArea;

import static javax.swing.JOptionPane.showMessageDialog;

import org.jfree.chart.ChartFactory;

import org.jfree.chart.ChartPanel;

import org.jfree.chart.JFreeChart;

import org.jfree.chart.axis.CategoryAxis;

import org.jfree.chart.axis.DateAxis;

import org.jfree.chart.axis.NumberAxis;

import org.jfree.chart.block.BlockBorder;

import org.jfree.chart.plot.CategoryPlot;

import org.jfree.chart.plot.PlotOrientation;

import org.jfree.chart.plot.XYPlot;

import org.jfree.chart.renderer.category.BarRenderer;

import org.jfree.chart.renderer.xy.XYItemRenderer;

import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import org.jfree.chart.renderer.xy.XYSplineRenderer;

import org.jfree.chart.title.TextTitle;

import org.jfree.chart.util.TableOrder;

import org.jfree.data.category.DefaultCategoryDataset;

import org.jfree.data.time.TimeSeries;

import org.jfree.data.time.TimeSeriesCollection;

import org.jfree.data.time.Year;

import org.jfree.data.xy.XYSeries;

import org.jfree.data.xy.XYSeriesCollection;



public class Selection implements ActionListener, ItemListener {

	ArrayList<JComboBox<String>> options;

	ArrayList<JButton> buttons;

	String countrySel;

	String fromSel;

	String toSel;

	String viewSel;

	ArrayList<String> viewSelist;

	String addButt;

	String remButt;

	String analySel;
	
	DropdownObserver observer = new DropdownObserver(null, null, null, null);



	public Selection() {

		options = new ArrayList<JComboBox<String>>();

		buttons = new ArrayList<JButton>();

		countrySel = null;

		fromSel = null;

		toSel = null;

		viewSel = null;

		viewSelist = new ArrayList<String>();

		analySel = null;

	}

	

	public ArrayList<JComboBox<String>> getChoices() {

		//Add Analysis options

		Vector<String> methodsNames = new Vector<String>();
		
		methodsNames.add("...");
		
		methodsNames.add("CO2 emissions vs Energy use vs PM2.5 air pollution");

		methodsNames.add("Average Forest area");

		methodsNames.add("PM2.5 air pollution vs Forest Area");

		methodsNames.add("Current health expenditure per capita vs Mortality rate, infant");

		methodsNames.add("Ratio of Hospital Beds vs Current Health Expenditure per 1000 people");

		methodsNames.add("Average of Government Expenditure on Education, total");

		methodsNames.add("Ratio of Government Expenditure on Education vs Current Health Expenditure");

		JComboBox<String> methodsList = new JComboBox<String>(methodsNames);

		analySel = (String) methodsList.getSelectedItem();
		
		observer.setAnalysis(analySel);

		options.add(methodsList);
		
		//Country Choices

		Vector<String> countriesNames = new Vector<String>();

		countriesNames.add("...");

		countriesNames.sort(null);

		JComboBox<String> countriesList = new JComboBox<String>(countriesNames);

		countrySel = (String) countriesList.getSelectedItem();
		
		observer.setCountry(countrySel);

		options.add(countriesList);

		
		//Viewer Choices

		Vector<String> viewsNames = new Vector<String>();

		viewsNames.add("...");

		JComboBox<String> viewsList = new JComboBox<String>(viewsNames);

		viewSel = (String) viewsList.getSelectedItem();

		options.add(viewsList);
				
		//Date Choices

		Vector<String> years = new Vector<String>();

		for (int i = 2021; i >= 2010; i--) {

			years.add("" + i);

		}

		JComboBox<String> fromList = new JComboBox<String>(years);

		JComboBox<String> toList = new JComboBox<String>(years);

		fromSel = (String) fromList.getSelectedItem();
		
		observer.setFrom(fromSel);

		toSel = (String) toList.getSelectedItem();
		
		observer.setTo(toSel);

		options.add(fromList);

		options.add(toList);

		

		return options;

	}



	//Add Viewers selected

	public void addView(JButton adds) {

		adds.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				viewSelist.add(viewSel);

				} 

		});

	}

	

	//Remove Viewers selected

	public void removeView(JButton rems) {

		rems.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				if(viewSelist.contains(viewSel)) {

					viewSelist.remove(viewSel);

				}else {

					showMessageDialog(null, "This Viewer was not previously chosen.");

				}

			}

		});

	}

	

	//Recalculate 

	public void recalculation(JButton recal) {

		recal.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {

				

			}

		});

	}

	public String getCountry() {

		return countrySel;

	}

	

	public String getAnalysis() {

		return analySel;

	}

	

	public String getFrom() {

		return fromSel;

	}

	

	public String getTo() {

		return toSel;

	}

	

	public ArrayList<String> getViewers(){

		return viewSelist;

	}

	

	public void componentResized(ComponentEvent e) {

		// TODO Auto-generated method stub

		

	}



	public void componentMoved(ComponentEvent e) {

		// TODO Auto-generated method stub

		

	}



	public void componentShown(ComponentEvent e) {

		// TODO Auto-generated method stub

		

	}



	public void componentHidden(ComponentEvent e) {

		// TODO Auto-generated method stub

		

	}



	public void itemStateChanged(ItemEvent e) {

		// TODO Auto-generated method stub

		

	}



	public void actionPerformed(ActionEvent e) {

		// TODO Auto-generated method stub

		

	}

	

}
