package httpTest;


import com.google.gson.JsonArray;

public class CO2VsEnergyUseVsAirPollution implements Analysis {

	public JsonArray CO2Json;
	public JsonArray EnergyJson;
	public JsonArray AirPollutionJson;

	public Double[] CO2;
	public Double[] energy;
	public Double[] pollution;

	public String country;
	public int from;
	public int to;

	public CO2VsEnergyUseVsAirPollution(JsonArray Co2, JsonArray energyUse, JsonArray airPollution) {
		CO2Json = Co2;
		EnergyJson = energyUse;
		AirPollutionJson = airPollution;
	}

	public void performAnalysis() throws Exception {
		int year;
		int sizeOfResults = CO2Json.get(1).getAsJsonArray().size();

		country = CO2Json.get(1).getAsJsonArray().get(0).getAsJsonObject().get("country").getAsJsonObject().get("value")
				.getAsString();

		from = CO2Json.get(1).getAsJsonArray().get(0).getAsJsonObject().get("date").getAsInt();
		to = CO2Json.get(1).getAsJsonArray().get(sizeOfResults - 1).getAsJsonObject().get("date").getAsInt();

		CO2 = new Double[sizeOfResults];
		energy = new Double[sizeOfResults];
		pollution = new Double[sizeOfResults];

		for (int i = 0; i < sizeOfResults; i++) {
			year = CO2Json.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();
			if (CO2Json.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()
					|| EnergyJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()
					|| AirPollutionJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {
				throw new Exception("no data for year: " + year);
			} else {
				CO2[i] = CO2Json.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").getAsDouble();
				energy[i] = EnergyJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").getAsDouble();
				pollution[i] = AirPollutionJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value")
						.getAsDouble();

			}
			System.out.println("year : 		" + year);
			System.out.println("CO2 : 		" + CO2[i]);
			System.out.println("Energy : 	" + energy[i]);
			System.out.println("Pollution : " + pollution[i]);

		}

	}
}