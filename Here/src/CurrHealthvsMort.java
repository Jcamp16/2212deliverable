package httpTest;



import com.google.gson.JsonArray;



public class CurrHealthvsMort implements Analysis{

	public JsonArray mortJson;

	public JsonArray healthJson;

	public Double[] mortality;

	public Double[] health;

	public String country;

	public int from;

	public int to;

	

	//constructor of the class. the CurrHealthvsMort class receives two json objects for which to analyze

	public CurrHealthvsMort(JsonArray mort, JsonArray health) {

		mortJson = mort;

		healthJson = health;

	}

	

	/*

	 * Processes the two json objects. This entails calculating required variables and identifying required values

	 * for the viewer to create its visualizations

	 * the AverageForestArea analysis type requires no calculations. Only for the following values to be identified:

	 * - Current health expenditure per capita (current US$) for each specified year -> currHealth

	 * - Mortality rate, infant for each specified year

	 * - country for which the data is on            -> country

	 * - start date from which data is pulled        -> from

	 * - end date from which data is pulled          -> to

	 */

	public void performAnalysis() throws Exception {

		int year;																											//variable to track the current year																											

		double currMort = 0;	

		double currHealth = 0;																				



		

		int sizeOfResults = mortJson.get(1).getAsJsonArray().size();

		mortality = new Double[sizeOfResults];

		health = new Double[sizeOfResults];

		country = mortJson.get(1).getAsJsonArray().get(0).getAsJsonObject()

				.get("country").getAsJsonObject().get("value").getAsString();

		from = mortJson.get(1).getAsJsonArray().get(0).getAsJsonObject()

				.get("date").getAsInt();

		to = mortJson.get(1).getAsJsonArray().get(sizeOfResults - 1)

				.getAsJsonObject().get("date").getAsInt();

		

		for (int i = 0; i < sizeOfResults; i++) {

			year = mortJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("date").getAsInt();

			if (mortJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull() || healthJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").isJsonNull()) {

				throw new Exception("no data for year: " + year);

			}

			else {

				currMort = mortJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").getAsDouble();

				currHealth = healthJson.get(1).getAsJsonArray().get(i).getAsJsonObject().get("value").getAsDouble();

			}

			mortality[i] = currMort;

			health[i] = currHealth;

			

			System.out.println("Current health expenditure per capita (current US$) vs Mortality rate, infant (per 1,000 live births) in " + year + " is: " + currHealth + " vs " + currMort);

		}

	}

}
