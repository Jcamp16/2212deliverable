package httpTest;

import javax.swing.JPanel;

public interface Viewer {
	JPanel west = null;
	Analysis analysis = null;
	
	public void setAnalysisType(Analysis _analysis);
	public void performAnalysis();
	
}
