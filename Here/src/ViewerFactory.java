package httpTest;

import javax.swing.JPanel;

public class ViewerFactory {
	public Viewer makeViewer(String newViewerType, JPanel west) {
		
		if(newViewerType.equals("TimeSeriesViewer")) {
			return new TimeSeriesViewer(west);
		}
		else if(newViewerType.equals("LineViewer")) {
			return new LineViewer(west);
		}
		else if(newViewerType.equals("BarViewer")) {
			return new BarViewer(west);
		}
		else if(newViewerType.equals("PieViewer")) {
			return new PieViewer(west);
		}
		else if(newViewerType.equals("ReportViewer")) {
			return new ReportViewer(west);
		}
		else {
			return null;
		}
	}
	public Viewer makeViewer(String newViewerType, JPanel west, Selection selChoices) {		
		if(newViewerType.equals("ScatterViewer")) {
			return new ScatterViewer(west, selChoices);
		}
		else {
			return null;
		}
	}
}
