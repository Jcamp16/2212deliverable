package httpTest;

import java.util.ArrayList;


public class DropdownObserver implements Observer {
	
	
    private ArrayList<String> observers;
    String count;
    String toYear;
    String fromYear;
    String analy;

    
    

	public DropdownObserver(String country, String from, String to, String analysis) {
		
		observers = new ArrayList<String>();
		count = country;
		fromYear = from;
		toYear = to;
		analy = analysis;
		observers.add(country);
		observers.add(from);
		observers.add(to);
		observers.add(analysis);
			

}
	
	public void updateCountry(String country) {
		observers.set(0, country);
		
	}


	public void updateFrom(String from) {
		observers.set(1, from);
		
	}


	public void updateTo(String to) {
		observers.set(2, to);
		
	}


	public void updateAnalysis(String analysis) {
		observers.set(3, analysis);
		
	}
		

	public void setCountry (String country) {
		count = country;
		updateCountry(country);
		
	}
	
	public void setFrom (String from) {
		fromYear = from;
		updateFrom(from);
	}
	
	public void setTo (String to) {
		toYear = to;
		updateTo(to);
	}

	public void setAnalysis (String analysis) {
		analy = analysis;
		updateAnalysis(analysis);
	}





}
