
public interface Analysis {
	
	void performAnalysis() throws Exception;
	
}
